# Какой запрос(-ы) отправляется на сервер для получения списка типов товаров в шторке "Каталог"?

POST
	https://megamarket.ru/api/mobile/v1/analyticsDataService/event/push  

GET https://main-cdn.sbermegamarket.ru/upload/mnt/fab4945f-2029-47ca-b24b-09fe0f32946c.jpg  

POST https://analytics.google.com/g/collect?v=2&tid=G-W49D2LL5S1&gtm=45je41a0v882103419z876236425&_p=1705248778967&gcd=11l1l1l1l1&dma=0&cid=945231084.1705218461&ul=ru-ru&sr=1344x756&dl=https%3A%2F%2Fmegamarket.ru%2F&sid=1705248778&sct=3&seg=1&dt=%D0%9C%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D0%BF%D0%BB%D0%B5%D0%B9%D1%81%20%D0%9C%D0%B5%D0%B3%D0%B0%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82%20-%20%D0%BC%D0%B5%D1%81%D1%82%D0%BE%20%D0%B2%D1%8B%D0%B3%D0%BE%D0%B4%D0%BD%D1%8B%D1%85%20%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BE%D0%BA&uid=cmhqp2mf2jgdmh4931h0&_s=13&tfd=1191984  

GET https://extra-cdn.sbermegamarket.ru/static/dist/images/googleplay.739a9a.svg  

POST https://megamarket.ru/api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=ik6HbSki65pPi1ILXPYiqwYB8R9tlCuhufPdOX%2FDtAtHxuvfu8moYOePEABLN4kcw21dFOXe1mCn7Xj3pLdU9LQAvfF%2BnrKCMiNd37hOLgorupSeELxaYjLNNLd06zmDK2gfKOWSNSiI%2FiYqWPn8KvxKmleYyRoObzZGSG0%3D  

GET https://main-cdn.sbermegamarket.ru/upload/mnt/c20802da-32d3-4fdd-92c4-a03292467741.png

# Какой запрос(-ы) отправляется на сервер при использовании поиска товаров в каталоге?

POST /api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=VEwr2yy1ws8m1jSFeSDE2CC1ujYVUZCfOwtltWC6oD2CoHeixzxEt0U9qaJ8m%2FUL8j21NyXzZC%2F82RWdTwzifg4X3y9b3y7f01QI363MgYKzWo2vvc8Czxqmq2Fmodu0QZOcfegWJbZkNPgxmaehpR%2BuW3qeKa9EVFSzht0%3D HTTP/2

POST /api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=LFg%2FsTUmEZBYzBcD64%2FJ29y2ZUlzCablFK5Ogg1re2lhTbvwtIiQ1l7CZ2fDwjaHrJ77R4yj4pDWrLxCLxy0FrfR2bYE5Y5OlXEl0EqFoW%2F7eFa6XqgJljiqyCxp3NEGJ3rIxVblGD6vIjZpCUtlVGyrEkI8cJA4shB7ZfE%3D HTTP/2

GET /tracker?_=0.4276679763368225;id=3376238;u=https%3A//megamarket.ru/catalog/vstraivaemye-mikrovolnovye-pechi/;st=1705236745248;title=%D0%92%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5%20%D0%BF%D0%B5%D1%87%D0%B8%20-%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%83%D1%8E%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D1%83%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B0%D1%85%2C%20%D1%86%D0%B5%D0%BD%D1%8B%20%D0%BD%D0%B0%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D0%B8%20(%D0%A1%D0%92%D0%A7%20%D0%BF%D0%B5%D1%87%D0%B8)%20%D0%B2%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B5%20%D0%BD%D0%B0%20%D0%9C%D0%B5%D0%B3%D0%B0%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82;s=1536*864;vp=1519*461;touch=0;hds=1.25;sid=a9fda2d481c80a3d;ver=60.3.0;tz=-180%2FEurope%2FMoscow;ct=3788/3792/3792/3800;gl=u;ni=;detect=0;lvid=1705160663021%3A1705238527451%3A10273%3Aa81ad3e185b8739e76c0bcdf59c6e4a8;opts=dl%2Cjst-gtag-ga-ym-vk;visible=false;js=13;e=RT/beat;ids=3376238%2C2856588%2C3223603%2C3215949 HTTP/2

POST /api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=zsBRHfye3tqsFgEsb1UT1oRPTJu%2FcqntRErhPXq5BkKwbL3CoRc2l7amqfekbUZhCQ0LMFgqMwBAJ9t5viSLZW9APhyjUqsM0GeUjmBSukhsSz84bNtCbBak3dSN3ygfhFQ3eJB%2BIgV7KH%2FEDHBOBMiJ%2B2gcJ6Oyylgk8ic%3D HTTP/2

GET /tracker?_=0.46552958075948436;id=3376238;u=https%3A//megamarket.ru/catalog/vstraivaemye-mikrovolnovye-pechi/;st=1705236745248;pid=USER_ID;title=%D0%92%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5%20%D0%BF%D0%B5%D1%87%D0%B8%20-%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%83%D1%8E%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D1%83%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B0%D1%85%2C%20%D1%86%D0%B5%D0%BD%D1%8B%20%D0%BD%D0%B0%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D0%B8%20(%D0%A1%D0%92%D0%A7%20%D0%BF%D0%B5%D1%87%D0%B8)%20%D0%B2%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B5%20%D0%BD%D0%B0%20%D0%9C%D0%B5%D0%B3%D0%B0%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82;s=1536*864;vp=1519*461;touch=0;hds=1.25;sid=a9fda2d481c80a3d;ver=60.3.0;tz=-180%2FEurope%2FMoscow;ct=3788/3792/3792/3800;gl=u;ni=;detect=0;lvid=1705160663021%3A1705238713070%3A10274%3Aa81ad3e185b8739e76c0bcdf59c6e4a8;opts=dl%2Cjst-gtag-ga-ym-vk;visible=false;js=13;e=RT/resend;et=1705236747630 HTTP/2

GET /tracker?_=0.5617742992240635;id=3215949;u=https%3A//megamarket.ru/catalog/vstraivaemye-mikrovolnovye-pechi/;st=1705236745248;title=%D0%92%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D1%8B%D0%B5%20%D0%BF%D0%B5%D1%87%D0%B8%20-%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%83%D1%8E%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D1%83%20%D0%B2%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD%D0%B0%D1%85%2C%20%D1%86%D0%B5%D0%BD%D1%8B%20%D0%BD%D0%B0%20%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%B8%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D0%B2%D0%BE%D0%BB%D0%BD%D0%BE%D0%B2%D0%BA%D0%B8%20(%D0%A1%D0%92%D0%A7%20%D0%BF%D0%B5%D1%87%D0%B8)%20%D0%B2%20%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B5%20%D0%BD%D0%B0%20%D0%9C%D0%B5%D0%B3%D0%B0%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82;s=1536*864;vp=1519*461;touch=0;hds=1.25;sid=a9fda2d481c80a3d;ver=60.3.0;tz=-180%2FEurope%2FMoscow;ct=3788/3792/3792/3800;gl=u;ni=;detect=0;lvid=1705160663021%3A1705238713074%3A10277%3Aa81ad3e185b8739e76c0bcdf59c6e4a8;opts=sec%2Cdl%2Cjst-gtag-ga-ym-vk;visible=false;js=13;e=RT/resend;et=1705236747630 HTTP/2

# Какой запрос(-ы) отправляется на сервер при поиске региона или города в модалке выбора вашего региона?

POST
	https://sentry-dsn-customers.megamarket.tech/api/6/envelope/?sentry_key=6d82cc11d08a4657a9092ff80b4bd615&sentry_version=7

POST
	https://megamarket.ru/api/fl?u=6efd37c0-6ff6-11ed-a2cf-b37c7b2b7873&cfidsw-smm=HmLctMWqWORH7U4tDyoorWQYBQM7zfd/E7ylbeZtrWdzWxluHvjMVhzXGz5Cp3zJNk11K5sYzNCPSvOGnx3hS8IWmQwPsuV3PpHYcS9h5vYPRAPIfFQSrfsHUTO4lRO3zUBJ8B8rbbG4KqMZBX0O2EBhmDGaILzPNjK+VXk=

GET
	https://crtrgt.bumlam.com/time/?localUid=778e6bd9-103c-44c1-9889-1af01fd60c67&pageID=67778c00-9045-4cd7-9e34-9c19cc18b4b2&time=264&cd=0.37793146141034617


GET
	https://mapgl.2gis.com/api/js

GET
	https://tile1-sdk.maps.2gis.com/vt?r=CggIugEQqwEYCA==&ts=vector_b&key=5df490c3-58d9-4fff-953b-96e5851b97fa&appId=empty&lang=ru&s=2c638fe6-f78b-4b1e-a6d8-26000dd68979

GET
	https://tile1-sdk.maps.2gis.com/v2/ald?ts=vector_immersive&x=23999&y=22146&z=15

