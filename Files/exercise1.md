# Кнопки

*Категория страхователя*
**Юридическое лицо**
**xPath** - //*[@id="mat-button-toggle-1-button"]/span

**Индивидуальный предприниматель**
**xPath** - //*[@id="mat-button-toggle-2-button"]

*Гражданство*
**Гражданин РФ**
**xPath** - //*[@id="mat-button-toggle-26-button"]/span

**Иностранный гражданин**
**xPath** - //*[@id="mat-button-toggle-27-button"]/span

*Пол*
**Мужской**
**xPath** - //*[@id="mat-button-toggle-28-button"]

**Женский**
**xPath** - //*[@id="mat-button-toggle-29-button"]

*Вид деятельности*
**Отрасль**
**xPath** - //*[@id="mat-select-value-1"]/span

**Вид деятельности**
**xPath** - //*[@id="mat-select-value-3"]/span

**Класс опасности**
**xPath** - //*[@id="mat-input-7"]

**Требование к АПС**
**xPath** -//*[@id="mat-input-8"]

**Наличие АПС**
**xPath** - //*[@id="mat-select-value-5"]/span

*Является законсервированным, под реконструкцией, незавершенным строительством*
**Да**
**xPath** - //*[@id="mat-button-toggle-10-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-11-button"]

*Является некапитальным зданием/сооружением без фундамента, воздухоопорной и/или надувной конструкцией*
**Да**
**xPath** - //*[@id="mat-button-toggle-12-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-13-button"]/span

*На объекте отсутствуют первичные средства пожаротушения (огнетушители / пожарный кран / пожарный водопровод) или их количество и состояние не соответствуют действующим нормам пожарной безопасности РФ*
**Да**
**xPath** - //*[@id="mat-button-toggle-14-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-15-button"]/span

*Имеет не исполненные предписания МЧС и/или Ростехнадзора на начало страхования*
**Да**
**xPath** - //*[@id="mat-button-toggle-16-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-17-button"]

*Расположено в зоне объявленной чрезвычайной ситуации*
**Да**
**xPath** - //*[@id="mat-button-toggle-18-button"]/span

**Нет**
**xPath** - //*[@id="mat-button-toggle-19-button"]

*Является рынком (совокупность отдельно стоящих или примыкающих друг к другу строений, расположенных на одной территории); отдельностоящей баней/сауной/бассейном; памятником истории/культуры/архитектуры*
**Да**
**xPath** - //*[@id="mat-button-toggle-20-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-21-button"]

*Имеется неузаконенная перепланировка / реконструкция объекта в части капитальной конструкции и инженерных систем*
**Да**
**xPath** - //*[@id="mat-button-toggle-22-button"]

**Нет**
**xPath** - //*[@id="mat-button-toggle-23-button"]/span

*Объект 1*
**Выбор категории объекта**
**xPath** - //*[@id="mat-select-value-15"]/span

**Наименование объекта**
**xPath** - //*[@id="mat-select-value-17"]/span

**Добавить объект**
**xPath** - //*[@class="mat-button-wrapper"][contains(., 'Добавить объект')]

**Удалить объект 1**
**xPath** - //*[contains(@class, 'mat-button-wrapper')][contains(normalize-space(.), 'Удалить объект 1')]

**Добавить территорию**
**xPath** - //*[contains(@class, 'mat-button-wrapper')][contains(., 'Добавить территорию')]

**Удалить территорию 1**
**xPath** - //*[contains(@class, 'mat-button-wrapper')][contains(normalize-space(.), 'Удалить территорию 1')]

*Залоговый договор 1*
**Добавить договор**
**xPath** - (//*[contains(@class, 'mat-button-wrapper') and contains(normalize-space(), 'Добавить договор')])[1]

*Кредитный договор*
**Добавить договор**
**xPath** - (//*[contains(@class, 'mat-button-wrapper') and contains(normalize-space(), 'Добавить договор')])[2]

**Рассчитать**
**xPath** - //*[contains(@class, 'mat-button-wrapper') and contains(normalize-space(), 'Рассчитать')]

# Поля для ввода текста

**Поиск по Наименованию/ИНН/ОГРН**  
**Xpath** - //*[@id="mat-input-21"]     

**Регион**   
**Xpath** - //*[@id="mat-input-10"]    

**Город**   
**Xpath** - //*[@id="mat-input-11"]  

**Улица**   
**Xpath** - //*[@id="mat-input-12"]  

**Дом**   
**Xpath** - //*[@id="mat-input-13"]  

**Офис, помещение, кадастровый номер**   
**Xpath** - //*[@id="mat-input-14"]  

**Класс опасности**   
**Xpath** - //*[@id="mat-input-7"]  

**Требование к АПС**   
**Xpath** - //*[@id="mat-input-8"]  

**Год постройки здания или последний кап ремонт**   
**Xpath** - //*[@id="mat-input-9"]  

**Расшифровка названия объекта**   
**Xpath** - //*[@id="mat-input-15"]  

**Офис/помещение**   
**Xpath** - //*[@id="mat-input-16"]  

**Кадастровый номер**   
**Xpath** - //*[@id="mat-input-17"]  

**Площадь объекта**   
**Xpath** - //*[@id="mat-input-18"]  

**Страховая сумма**   
**Xpath** - //*[@id="mat-input-19"]  

**Страховая стоимость**   
**Xpath** - //*[@id="mat-input-20"]    

**Телефон**   
**Xpath** - //*[@id="mat-input-1"]   

**Электронная почта**   
**Xpath** - //*[@id="mat-input-2"]   

**Дополнительная электронная почта**   
**Xpath** - //*[@id="mat-input-3"]    

**Франшиза**   
**Xpath** - //*[@id="mat-input-4"]  

**Введите промокод**   
**Xpath** - //*[@id="mat-input-5"]

**Страна регистрации**   
**Xpath** - //*[@id="mat-input-22"]    

**Полное наименование**   
**Xpath** - //*[@id="mat-input-23"]  

**ИНН**   
**Xpath** - //*[@id="mat-input-24"]  

**КПП**   
**Xpath** - //*[@id="mat-input-25"]  

**ОГРН**   
**Xpath** - //*[@id="mat-input-26"]  

**Регион**   
**Xpath** - //*[@id="mat-input-27"]    

**Город или населенный пункт**   
**Xpath** - //*[@id="mat-input-28"]  

**Улица**   
**Xpath** - //*[@id="mat-input-29"]  

**Дом, литер, корпус**   
**Xpath** - //*[@id="mat-input-30"]  

**Офис, помещение**   
**Xpath** - //*[@id="mat-input-31"]  

**Поиск по ФИО/ИНН/ОГРНИП**   
**Xpath** - //*[@id="mat-input-32"]  

**Фамилия**   
**Xpath** - //*[@id="mat-input-33"]  

**Имя**   
**Xpath** - //*[@id="mat-input-34"]  

**Отчество**   
**Xpath** - //*[@id="mat-input-35"]  

**ИНН**   
**Xpath** - //*[@id="mat-input-37"]  

**ОГРНИП**   
**Xpath** - //*[@id="mat-input-38"]  

**Регион**   
**Xpath** - //*[@id="mat-input-39"]  

**Город или населенный пункт**   
**Xpath** - //*[@id="mat-input-40"]  

**Улица**   
**Xpath** - //*[@id="mat-input-41"]  

**Дом, литер, корпус**   
**Xpath** - //*[@id="mat-input-42"]  

**Офис, помещение**   
**Xpath** - //*[@id="mat-input-43"]  

# Чекбоксы

**Залог Сбербанка**
**xPath** - //*[@id="mat-checkbox-1"]

**Заполнить вручную**
**xPath** - //*[@id="mat-checkbox-2"]

**Улица отсутствует**
**xPath** - //*[@id="mat-checkbox-17"]

**Отчество отсутствует**
**xPath** - //*[@id="mat-checkbox-19"]

**Адрес имущества совпадает с адресом регистрации**
**xPath** - //*[@id="mat-checkbox-3"]

**Улица отсутствует**
**xPath** - //*[@id="mat-checkbox-4"]

**Внезапные и непредвиденные расходы по расчистке территории от последствий возникновения ущерба**
**xPath** - //*[@id="mat-checkbox-10"]

**Внезапные и непредвиденные расходы по ограждению, укреплению, обшивке материалами, герметизации или поддержке зданий, сооружений или застрахованного имущества для обеспечения их безопасности**
**xPath** - //*[@id="mat-checkbox-11"]

**Расходы на перемещение и защиту**
**xPath** - //*[@id="mat-checkbox-12"]

**Расходы по благоустройству территории**
**xPath** - //*[@id="mat-checkbox-13"]

**Расходы по замене ключей, замков, мастер-ключей и т. д. (в случае их хищения)**
**xPath** - //*[@id="mat-checkbox-14"]

**Покрытие по риску терроризм**
**xPath** - //*[@id="mat-checkbox-5"]

**Покрытие по риску диверсия**
**xPath** - //*[@id="mat-checkbox-6"]

**Покрытие по риску народные волнения**
**xPath** - //*[@id="mat-checkbox-7"]

**Покрытие по риску бой стекол**
**xPath** - //*[@id="mat-checkbox-8"]

**Покрытие по риску не капитальный ремонт**
**xPath** - //*[@id="mat-checkbox-9"]


# Датапикеры

**Дата рождения**
**xPath** - //*[@id="mat-input-74"]

**Дата начала страхования**	
**xPath** - //*[@id="mat-input-0"]

# Логотип "СБЕР СТРАХОВАНИЕ
**xPath** - //header[@class="header container-gt-xs"]

# Слайдер в блоке выбора срока страхования
**xPath** - //*[@id="mat-input-6"]

# Хедер "Данные страхователя"	
**xPath** - //h4[@class="block-header" and contains(text(), "Данные страхователя")]
	
# "Укажите дату начала действия полиса и срока страхования"	
**xPath** - //p[@class="date-police"]
	
# "Является законсервированным, под реконструкцией, незавершенным строительством"	
**xPath** - //span[@class="stop1"]
